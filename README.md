# ASL Robotic Hand

The combination of InMoov Robotics with ASL presents a prototype of a robotic prosthetic arm that is dexterous to perform basic ASL. 

More infomation, media and project description on my personal website: https://sagemason47.wixsite.com/mysite/robotichand-project


This is still a work in progress. More functionality to be added soon. 

Below is a short description of the hardware used:
* InMoov Robotic hand                    http://inmoov.fr/hand-and-forarm/
* Raspberry Pi 3+
* 16 Ch. PWM Driver board PCA9685:       https://www.adafruit.com/product/815

To perform ASL, run the cylce_all script.